<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Exceptions;

/**
 * Class InvalidInheritedArgumentException
 * @package JulienCoppin\GlobalBundle\Exceptionss
 */
class InvalidInheritedArgumentException extends \Exception
{
    /**
     * InvalidInheritedArgumentException constructor.
     * @param string $class
     * @param string $toImplement
     */
    public function __construct(string $class, string $toImplement)
    {
        if (!is_string($class)) {
            $class = get_class($class);
        }
        parent::__construct(sprintf("%s does not implement/inherited %s", $class, $toImplement));
    }
}