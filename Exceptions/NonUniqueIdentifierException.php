<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Exceptions;

/**
 * Class NonUniqueIdentifierException
 * @package JulienCoppin\GlobalBundle\Exceptions
 */
class NonUniqueIdentifierException extends \Exception
{
    /**
     * NonUniqueIdentifierException constructor.
     * @param object|string $object
     */
    public function __construct($object)
    {
        parent::__construct(sprintf("Incorrect identifier for %s supplied", is_object($object) ? get_class($object) : $object));
    }
}