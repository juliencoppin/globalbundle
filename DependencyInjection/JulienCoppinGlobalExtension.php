<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Form\Exception\InvalidConfigurationException;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Class JulienCoppinGlobalExtension
 * @package JulienCoppin\GlobalBundle\DependencyInjection
 */
class JulienCoppinGlobalExtension extends Extension
{
    /**
     * @param array $configs
     * @param ContainerBuilder $container
     */
    public function load(array $configs, ContainerBuilder $container) : void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        if (!isset($config["routine"], $config["maintenance"])) {
            throw new InvalidConfigurationException("The configuration for julien_coppin_global is invalid");
        }

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $definitionRoutine = $container->getDefinition("julien_coppin_global.config_routine");
        $definitionRoutine->addMethodCall('setConfig', array($config["routine"]));

        $definitionMaintenance = $container->getDefinition("julien_coppin_global.config_maintenance");
        $definitionMaintenance->addMethodCall('setConfig', array($config["maintenance"]));
    }
}
