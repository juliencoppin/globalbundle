<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 * @package JulienCoppin\GlobalBundle\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder() : TreeBuilder
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('julien_coppin_global');

        $rootNode
            ->children()
                ->arrayNode('maintenance')
                    ->isRequired()
                    ->cannotBeEmpty()
                    ->children()
                        ->arrayNode("routes")
                            ->prototype("scalar")
                            ->end()
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                        ->arrayNode("roles")
                            ->prototype("scalar")
                            ->end()
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('routine')
                    ->isRequired()
                    ->prototype("scalar")
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
