<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Interfaces;

use JulienCoppin\GlobalBundle\Entity\CronTask;

/**
 * Interface ICronTask
 * @package JulienCoppin\GlobalBundle\Interfaces
 */
interface ICronTask
{
    /**
     * @param CronTask $cronTask
     * @param \DateTime $runTime
     */
    public function run(CronTask $cronTask, \DateTime $runTime) : void;
}