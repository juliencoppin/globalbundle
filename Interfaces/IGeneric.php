<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Interfaces;

/**
 * Interface IGeneric
 * @package JulienCoppin\GlobalBundle\Interfaces
 */
interface IGeneric
{
    public function validateNames() : void;

    public function setRequiredNames() : void;
}