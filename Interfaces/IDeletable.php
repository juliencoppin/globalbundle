<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Interfaces;

/**
 * Interface IDeletable
 * @package JulienCoppin\GlobalBundle\Interfaces
 */
interface IDeletable
{
    /**
     * @return bool
     */
    public function isDeletable() : bool;
}