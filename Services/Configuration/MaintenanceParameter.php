<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Services\Configuration;

/**
 * Class MaintenanceParameter
 * @package JulienCoppin\GlobalBundle\Services\Configuration
 */
class MaintenanceParameter
{
    /**
     * @var array
     */
    private $routes;

    /**
     * @var array
     */
    private $roles;

    /**
     * @param array $config
     */
    public function setConfig(array $config) : void
    {
        $this->routes = $config["routes"];
        $this->roles = $config["roles"];
    }

    /**
     * @return array
     */
    public function getRoutes() : array
    {
        return $this->routes;
    }

    /**
     * @return array
     */
    public function getRoles() : array
    {
        return $this->roles;
    }
}