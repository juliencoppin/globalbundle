<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Services\Configuration;

/**
 * Class RoutineParameter
 * @package JulienCoppin\GlobalBundle\Service\Configuration
 */
class RoutineParameter
{
    /**
     * @var array
     */
    private $routines;

    /**
     * @param array|null $config
     */
    public function setConfig(array $config) : void
    {
        $this->routines = $config;
    }

    /**
     * @return array
     */
    public function getRoutines() : array
    {
        return $this->routines;
    }
}