<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 25-05-16
 * Time: 12:46
 */

namespace JulienCoppin\GlobalBundle\Services;

use JulienCoppin\GlobalBundle\Helpers\MaintenanceHelper;
use JulienCoppin\GlobalBundle\Services\Configuration\MaintenanceParameter;
use JulienCoppin\GlobalBundle\Services\Configuration\RedirectLoginParameter;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class KernelListener
{
    /**
     * @var AuthorizationChecker
     */
    private $security;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var MaintenanceParameter
     */
    private $maintenanceParams;

    /**
     * @var Maintenance
     */
    private $maintenanceService;

    /**
     * KernelListener constructor.
     * @param ContainerInterface $container
     * @param Maintenance $maintenanceService
     */
    public function __construct(ContainerInterface $container, Maintenance $maintenanceService)
    {
        $this->security = $container->get('security.authorization_checker');
        $this->router = $container->get('router');
        $this->tokenStorage = $container->get('security.token_storage');
        $this->maintenanceParams = $container->get('juliencoppin_core.config_maintenance');
        $this->maintenanceService  = $maintenanceService;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event) : void
    {
        $route = $event->getRequest()->attributes->get('_route');

        $maintenance = $this->maintenanceService->getMaintenance();
        if (filter_var($maintenance->getValue(), FILTER_VALIDATE_BOOLEAN) === true){
            if (!in_array($route, $this->maintenanceParams->getRoutes())){
                if ($this->tokenStorage->getToken() === null || !$this->security->isGranted($this->maintenanceParams->getRoles())) {
                    $event->setResponse(new JsonResponse('Maintenance', Response::HTTP_SERVICE_UNAVAILABLE));
                    return;
                }
            }
        }
    }
}