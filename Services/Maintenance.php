<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Services;
use Doctrine\Common\Persistence\ObjectManager;
use JulienCoppin\GlobalBundle\Entity\GlobalParameter;

/**
 * Class Maintenance
 * @package JulienCoppin\GlobalBundle\Services
 */
class Maintenance
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * Maintenance constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @return GlobalParameter
     */
    public function getMaintenance() : GlobalParameter
    {
        /** @var GlobalParameter $item */
        $item = $this->objectManager->getRepository('JulienCoppinGlobalBundle:GlobalParameter')->findOneBy(array('globalParameterLabel' => 'maintenance'));
        if ($item === null) {
            $item = new GlobalParameter();
            $item->setLabel('maintenance');
            $item->setValue(false);
            $this->objectManager->persist($item);
            $this->objectManager->flush();
        }
        return $item;
    }
}