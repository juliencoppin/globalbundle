<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Services;

use Doctrine\ORM\EntityManager;
use JulienCoppin\GlobalBundle\Entity\CronTask;
use JulienCoppin\GlobalBundle\Entity\CronTaskHistory;
use JulienCoppin\GlobalBundle\Interfaces\ICronTask;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Dispatcher
 * @package JulienCoppin\GlobalBundle\Services
 */
class Dispatcher
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Dispatcher constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param CronTask $cronTask
     * @param \DateTime $runTime
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    public function dispatchCronTask(CronTask $cronTask, \DateTime $runTime,InputInterface $input, OutputInterface $output) : void
    {
        if (!$this->isRunnable($cronTask, $runTime)) {
            return;
        }

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine')->getManager();

        if (!$this->container->has($cronTask->getServiceName())) {
            $this->generateHistory($em, $cronTask, $runTime, false, sprintf("Unknow service name : %s", $cronTask->getServiceName()));
            return;
        }

        try {
            $service = $this->container->get($cronTask->getServiceName());
            if (!$service instanceof ICronTask) {
                $this->generateHistory($em, $cronTask, $runTime, false, sprintf("%s is not an instance of %s", get_class($service), ICronTask::class));
                return;
            }
            $output->writeln(sprintf("Cron task started : %s", $cronTask->getName()));
            $start = microtime();
            $service->run($cronTask, $runTime);
            $end = microtime();
            $this->generateHistory($em, $cronTask, $runTime);
            $output->writeln(sprintf("Cron task ended : %s <info>(Execution time : %s ms)</info>", $cronTask->getName(), number_format($end - $start, 2, ',', '.')));
        } catch (\Exception $e) {
            $this->generateHistory($em, $cronTask, $runTime, false, $e->getMessage());
            $output->writeln(sprintf("An error occured. Please check the cron's history before doing anything ..."));
        }
    }

    /**
     * @param CronTask $cronTask
     * @param \DateTime $runTime
     * @return bool
     */
    private function isRunnable(CronTask $cronTask, \DateTime $runTime) : bool
    {
        if (strcmp($cronTask->getRunTime()->format('H:i'), $runTime->format('H:i')) == 0) {
            if ($cronTask->getRunDayOfMonth() !== null && $cronTask->getRunDayOfWeek() !== null) {
                return false;
            }
            else if ($cronTask->getRunDayOfMonth() !== null) {
                return ($cronTask->getRunDayOfMonth() == (int)$runTime->format('d'));
            } else if ($cronTask->getRunDayOfWeek() !== null) {
                return ($cronTask->getRunDayOfWeek() == (int)$runTime->format('N'));
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * @param EntityManager $em
     * @param CronTask $cronTask
     * @param \DateTime $runTime
     * @param bool $result
     * @param null|string $message
     */
    private function generateHistory(EntityManager &$em, CronTask $cronTask, \DateTime $runTime, bool $result = true, ?string $message = null) : void
    {
        $history = new CronTaskHistory();
        $history->setCronTask($cronTask);
        $history->setStartRunTime($runTime);
        $history->setResult($result);
        $history->setExceptionMessage($message);
        $em->persist($history);
        $em->flush();
    }
}