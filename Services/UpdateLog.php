<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Services;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use JulienCoppin\GlobalBundle\Entity\Log;
use JulienCoppin\GlobalBundle\Interfaces\ILog;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class UpdateLog
{
    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var Session
     */
    private $session;

    /**
     * UpdateLog constructor.
     * @param TokenStorage $tokenStorage
     * @param Session $session
     */
    public function __construct(TokenStorage $tokenStorage, Session $session)
    {
        $this->tokenStorage = $tokenStorage;
        $this->session = $session;
    }

    /**
     * @param PreUpdateEventArgs $args
     */
    public function preUpdate(PreUpdateEventArgs $args) : void
    {
        $entity = $args->getEntity();
        if ($entity instanceof ILog) {
            $data = $args->getEntityChangeSet();
            if (count($data) > 0) {
                $em = $args->getEntityManager();
                $token = $this->tokenStorage->getToken();
                $user = isset($token) ? $token->getUser() : null;
                if ($user !== null) {
                    if (strcmp(gettype($user), "string") === 0) {
                        $email = $user;
                    } elseif ($user instanceof \Symfony\Component\Security\Core\User\User) {
                        $email = $user->getUsername();
                    } else {
                        $email = 'localhost : ' . get_class($user);
                    }
                } else {
                    $email = 'localhost';
                }
                $Logs = array();
                foreach ($data as $field => $items) {
                    $Log = new Log();
                    $Log->setTable($args->getEntityManager()->getClassMetadata(get_class($entity))->getTableName());
                    $Log->setEmail($email);
                    $Log->setField($field);
                    $Log->setOldValue($this->setData($items[0], $em));
                    $Log->setNewValue($this->setData($items[1], $em));
                    array_push($Logs, $Log);
                }
                $this->session->set('Logs', $Logs);
            }
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args) : void
    {
        $Logs = $this->session->get('Logs');
        if (isset($Logs) && !empty($Logs) && $args->getEntity() instanceof ILog) {
            $em = $args->getEntityManager();
            foreach ($Logs as $log) {
                $em->persist($log);
            }
            $em->flush();
            $this->session->remove('Logs');
        }
    }

    /**
     * @param $data
     * @param EntityManager $em
     * @return string
     */
    private function setData($data, EntityManager $em) : string
    {
        if ($data instanceof \DateTime) {
            return $data->format('d-m-Y H:i:s');
        } else if (is_object($data)) {
            $methodName = sprintf("get%s", ucfirst($em->getClassMetadata(get_class($data))->getIdentifier()[0]));
            return (string)$data->{$methodName}();
        }
        return $data;
    }
}