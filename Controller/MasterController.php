<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 19-05-16
 */

namespace JulienCoppin\GlobalBundle\Controller;

use Doctrine\ORM\EntityManager;
use JulienCoppin\GlobalBundle\Exceptions\BreadcrumbException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

abstract class MasterController extends Controller
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Translator
     */
    protected $translator;

    /**
     * @param null|ContainerInterface $container
     */
    public function setContainer(?ContainerInterface $container) : void
    {
        parent::setContainer($container);

        $this->em = $container->get('doctrine')->getManager();
        $this->translator = $container->get('translator');

        $this->initFields();
    }

    protected function initFields() : void
    {
    }

    /**
     * @param Request $request
     */
    protected function rejectNonXHR(Request $request) : void
    {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequestHttpException('Call should be XHR');
        }
    }

    /**
     * @param $item
     */
    protected function exceptionIfNull($item) : void
    {
        if (null === $item) {
            throw $this->createNotFoundException();
        }
    }
}