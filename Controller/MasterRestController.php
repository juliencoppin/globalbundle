<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Controller;

use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class MasterRestController
 * @package JulienCoppin\GlobalBundle\Controller
 */
class MasterRestController extends MasterController
{
    /**
     * @param $resourceName
     * @return View
     */
    final protected function generateMissingResource(string $resourceName) : View
    {
        return View::create(array('message' => sprintf('Missing resource : %s', $resourceName)), Response::HTTP_NOT_FOUND);
    }
}