<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Command;

use Doctrine\ORM\EntityManager;
use JulienCoppin\GlobalBundle\Entity\CronTask;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CronTaskCommand
 * @package JulienCoppin\GlobalBundle\Command
 */
class CronTaskCommand extends ContainerAwareCommand
{
    protected function configure() : void
    {
        $this->setName("juliencoppin:cron:run")
            ->setDescription("Start the crontasks");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output) : void
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $runTime = new \DateTime();

        $cronTasks = $em->getRepository('JulienCoppinGlobalBundle:CronTask')->findAllActive();
        $dispatcher = $this->getContainer()->get('julien_coppin_global.dispatch');

        $output->writeln("Running cron tasks ...");

        /** @var CronTask $cronTask */
        foreach ($cronTasks as $cronTask) {
            $dispatcher->dispatchCronTask($cronTask, $runTime, $input, $output);
        }
    }
}