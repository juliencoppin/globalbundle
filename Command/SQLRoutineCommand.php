<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SQLRoutineCommand
 * @package JulienCoppin\GlobalBundle\Command
 */
class SQLRoutineCommand extends ContainerAwareCommand
{
    protected function configure() : void
    {
        $this->setName('juliencoppin:sql:load');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output) : void
    {
        $container = $this->getContainer();
        /** @var EntityManager $em */

        $em = $container->get('doctrine')->getManager();
        $connection = $em->getConnection();
        $pdo = new \PDO(sprintf("mysql:host=%s;dbname=%s", $connection->getHost(), $connection->getDatabase()), $connection->getUsername(), $connection->getPassword());
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        $nbOfScriptsExecuted = 0;

        $paths = $container->get('juliencoppin_custom.config_routine')->getRoutines();
        $rootDir = $container->getParameter('kernel.root_dir');

        foreach ($paths as $path) {
            $path = realpath(join(DIRECTORY_SEPARATOR, array($rootDir, '..', $path)));
            if ($path === false) {
                throw new \Exception('Invalid path value');
            }

            foreach (scandir($path) as $item) {
                $itemPath = join(DIRECTORY_SEPARATOR, array($path, $item));
                if (!is_dir($itemPath)) {
                    $extension = pathinfo($itemPath, PATHINFO_EXTENSION);
                    if (strcmp(strtolower($extension), 'sql') === 0) {
                        $output->writeln(sprintf("Running script : <info>%s</info>", $item));
                        $query = file_get_contents($itemPath);
                        $qb = $pdo->prepare($query);
                        $qb->execute();
                        $nbOfScriptsExecuted++;
                    }
                }
            }
        }
        $output->writeln(sprintf("Number of sql scripts executed : <info>%s</info>", $nbOfScriptsExecuted));
    }
}