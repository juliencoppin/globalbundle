<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Form;

use JulienCoppin\GlobalBundle\Interfaces\IGeneric;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CustomFormType
 * @package JulienCoppin\GlobalBundle\Form
 */
abstract class CustomFormType extends AbstractType implements IGeneric
{
    /**
     * @var string
     */
    protected $data_class;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var bool
     */
    protected $ignoreDataClass = false;

    /**
     * ParameterFormType constructor.
     */
    public function __construct()
    {
        $this->setRequiredNames();
        $this->validateNames();
    }

    public function validateNames() : void
    {
        if (!isset($this->data_class) && !$this->ignoreDataClass)
        {
            throw new \Exception('missing data_class');
        }

        if (!isset($this->name))
        {
            throw new \Exception('missing form_name');
        }
    }

    public abstract function setRequiredNames() : void;

    /**
     * @return string
     */
    public function getBlockPrefix() : string
    {
        return $this->name;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function setDefaultsOptions(OptionsResolver $resolver) : void
    {
        if ($this->data_class !== null) {
            $resolver->setDefaults(array(
              'data_class' => $this->data_class,
            ));
        }
    }
}