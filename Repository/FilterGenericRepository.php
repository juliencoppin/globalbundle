<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * Class FilterGenericRepository
 * @package JulienCoppin\GlobalBundle\Repository
 */
class FilterGenericRepository extends EntityRepository
{
    /**
     * @var int
     */
    private $indexParam = 0;

    /** @var string */
    const PARAM_NAME = "param";

    /**
     * @param QueryBuilder $qb
     * @param string $param
     * @param string $field
     */
    protected function addTextParameter(QueryBuilder $qb, string $param, string $field) : void
    {
        if ($param === null) {
            return;
        }

        $paramAlias = self::PARAM_NAME . $this->indexParam;
        $request = sprintf("%s LIKE :%s", $field, $paramAlias);

        $qb->andWhere($request)
            ->setParameter($paramAlias, "%" . $param . "%");

        $this->indexParam++;
    }

    /**
     * @param QueryBuilder $qb
     * @param ArrayCollection $param
     * @param string $field
     */
    protected function addEntityParameter(QueryBuilder $qb, ArrayCollection $param, string $field) : void
    {
        if (count($param) == 0) {
            return;
        }

        $paramAlias = self::PARAM_NAME . $this->indexParam;

        $qb->andWhere(sprintf("%s IN (:%s)", $field, $paramAlias))
            ->setParameter($paramAlias, $param);

        $this->indexParam++;
    }

    /**
     * @param QueryBuilder $qb
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param string $field
     */
    protected function addBetweenDateParameter(QueryBuilder $qb, \DateTime $startDate, \DateTime $endDate, string $field) : void
    {
        if ($startDate !== null) {
            if ($startDate instanceof \DateTime) {
                $startDate->setTime(0, 0, 0);
            }
            $paramAlias = self::PARAM_NAME . $this->indexParam;
            $qb->andWhere(sprintf("DATE(%s) >= DATE(:%s)", $field, $paramAlias))
                ->setParameter($paramAlias, $startDate);
            $this->indexParam++;
        }

        if ($endDate !== null) {
            if ($endDate instanceof \DateTime) {
                $endDate->setTime(23, 59, 59);
            }
            $paramAlias = self::PARAM_NAME . $this->indexParam;
            $qb->andWhere(sprintf("DATE(%s) <= DATE(:%s)", $field, $paramAlias))
                ->setParameter($paramAlias, $endDate);
            $this->indexParam++;
        }
    }

    /**
     * @param QueryBuilder $qb
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param array $fields
     */
    protected function addBetweenDateWithMultipleFieldsParameter(QueryBuilder $qb, \DateTime $startDate, \DateTime $endDate, array $fields) : void
    {
        if ($startDate !== null) {
            $paramAlias = self::PARAM_NAME . $this->indexParam;
            $qb->andWhere($this->generateQueryWithMultipleValues($paramAlias, $fields, ">=", 'DATE'))
                ->setParameter($paramAlias, $startDate);
            $this->indexParam++;
        }

        if ($endDate !== null) {
            $paramAlias = self::PARAM_NAME . $this->indexParam;
            $qb->andWhere($this->generateQueryWithMultipleValues($paramAlias, $fields, "<=", 'DATE'))
                ->setParameter($paramAlias, $endDate);
            $this->indexParam++;
        }
    }

    /**
     * @param string $paramAlias
     * @param array $fields
     * @param string $operator
     * @param string $sqlFunc
     * @return string
     */
    private function generateQueryWithMultipleValues(string $paramAlias, array $fields, string $operator, string $sqlFunc) : string
    {
        $colName = $this->updateFormat("%s", $sqlFunc);
        $paramName = $this->updateFormat(":%s", $sqlFunc);
        $query = "";
        for ($i = 0; $i < count($fields); $i++) {
            if ($i < count($fields) - 1) {
                $query .= sprintf($colName . " %s " . $paramName .  " OR ", $fields[$i], $operator, $paramAlias);
            } else {
                $query .= sprintf($colName . " %s " . $paramName, $fields[$i], $operator, $paramAlias);
            }
        }
        return $query;
    }

    /**
     * @param string $formatString
     * @param string $sqlFunc
     * @return string
     */
    private function updateFormat(string $formatString, string $sqlFunc) : string
    {
        return $sqlFunc === null ? $formatString : $sqlFunc . '(' . $formatString . ')';
    }

    /**
     * @param QueryBuilder $qb
     * @param string $param
     * @param string $field
     */
    protected function addRadioParameter(QueryBuilder $qb, string $param, string $field) : void
    {
        $value = $this->getRadioParameterAsBoolean($param);

        if ($value === null) {
            return;
        }

        $paramAlias = self::PARAM_NAME . $this->indexParam;

        $qb->andWhere(sprintf("%s = :%s", $field, $paramAlias))
            ->setParameter($paramAlias, $value);

        $this->indexParam++;
    }

    /**
     * @param string $param
     * @return bool|null
     */
    private function getRadioParameterAsBoolean(string $param) : ?bool
    {
        return strcmp($param, 'active') == 0 ? true : (strcmp($param, 'inactive') == 0 ? false : null);
    }
}