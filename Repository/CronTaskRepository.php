<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Repository;

use Doctrine\ORM\EntityRepository;
use JulienCoppin\GlobalBundle\Entity\CronTask;

/**
 * Class CronTaskRepository
 * @package JulienCoppin\GlobalBundle\Repository
 */
class CronTaskRepository extends EntityRepository
{
    /**
     * @return CronTask[]
     */
    public function findAllActive()
    {
        $qb = $this->createQueryBuilder('ct')
            ->where('ct.cronTaskActive = TRUE');

        return $qb->getQuery()->getResult();
    }
}