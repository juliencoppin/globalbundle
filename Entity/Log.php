<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Log
 * @package JulienCoppin\GlobalBundle\Entity
 *
 * @ORM\Table(name="Logs")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
final class Log
{
    /**
     * @var integer
     *
     * @ORM\Column(name="LogID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="LogCreationDate", type="datetime", nullable=false)
     */
    private $creationDate;

    /**
     * @var string
     *
     * @ORM\Column(name="LogEmail", type="string", nullable=false, length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="LogTable", type="string", nullable=false, length=255)
     */
    private $table;

    /**
     * @var string
     *
     * @ORM\Column(name="LogField", type="string", nullable=false, length=255)
     */
    private $field;

    /**
     * @var string
     *
     * @ORM\Column(name="LogOldValue", type="text", nullable=true)
     */
    private $oldValue;

    /**
     * @var string
     *
     * @ORM\Column(name="LogNewValue", type="text", nullable=true)
     */
    private $newValue;

    /**
     * @var string
     *
     * @ORM\Column(name="LogIP", type="string", nullable=true, length=255)
     */
    private $ip;

    /**
     * Log constructor.
     */
    public function __construct()
    {
        $this->ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
    }

    /**
     * @ORM\PrePersist()
     */
    public function initDate()
    {
        $this->creationDate = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Log
     */
    public function setId(int $id): Log
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getCreationDate(): \Datetime
    {
        return $this->creationDate;
    }

    /**
     * @param \Datetime $creationDate
     * @return Log
     */
    public function setCreationDate(\Datetime $creationDate): Log
    {
        $this->creationDate = $creationDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Log
     */
    public function setEmail(string $email): Log
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getTable(): string
    {
        return $this->table;
    }

    /**
     * @param string $table
     * @return Log
     */
    public function setTable(string $table): Log
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @param string $field
     * @return Log
     */
    public function setField(string $field): Log
    {
        $this->field = $field;
        return $this;
    }

    /**
     * @return string
     */
    public function getOldValue(): string
    {
        return $this->oldValue;
    }

    /**
     * @param string $oldValue
     * @return Log
     */
    public function setOldValue(string $oldValue): Log
    {
        $this->oldValue = $oldValue;
        return $this;
    }

    /**
     * @return string
     */
    public function getNewValue(): string
    {
        return $this->newValue;
    }

    /**
     * @param string $newValue
     * @return Log
     */
    public function setNewValue(string $newValue): Log
    {
        $this->newValue = $newValue;
        return $this;
    }

    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     * @return Log
     */
    public function setIp(string $ip): Log
    {
        $this->ip = $ip;
        return $this;
    }
}
