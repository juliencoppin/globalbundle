<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class GlobalParameter
 * @package JulienCoppin\GlobalBundle\Entity
 *
 * @ORM\Table(name="GlobalParameters")
 * @ORM\Entity
 */
final class GlobalParameter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="GlobalParameterID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="GlobalParameterLabel", type="string", nullable=false, length=255, unique=true)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="GlobalParameterValue", type="text", nullable=false)
     */
    private $value;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return GlobalParameter
     */
    public function setId(int $id): GlobalParameter
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return GlobalParameter
     */
    public function setLabel(string $label): GlobalParameter
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return GlobalParameter
     */
    public function setValue($value): GlobalParameter
    {
        $this->value = (string)$value;
        return $this;
    }
}
