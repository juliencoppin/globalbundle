<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class CronTaskHistory
 * @package JulienCoppin\GlobalBundle\Entity
 *
 * @ORM\Table(name="CronTasksHistory")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
final class CronTaskHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="CronTaskHistoryID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="CronTaskHistoryStartRunTime", type="datetime", nullable=false)
     */
    private $startRunTime;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="CronTaskHistoryEndRunTime", type="datetime", nullable=false)
     */
    private $endRunTime;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CronTaskHistoryResult", type="boolean", nullable=false)
     */
    private $result;

    /**
     * @var string
     *
     * @ORM\Column(name="CronTaskHistoryExceptionMessage", type="text", nullable=true)
     */
    private $exceptionMessage;

    /**
     * @var CronTask
     *
     * @ORM\ManyToOne(targetEntity="JulienCoppin\GlobalBundle\Entity\CronTask")
     * @ORM\JoinColumn(name="CronTaskID", referencedColumnName="CronTaskID", nullable=false)
     */
    private $cronTask;

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function initDates()
    {
        if ($this->startRunTime === null) {
            $this->startRunTime = new \DateTime();
        }

        if ($this->endRunTime === null) {
            $this->endRunTime = new \DateTime();
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return CronTaskHistory
     */
    public function setId(int $id): CronTaskHistory
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getStartRunTime(): \Datetime
    {
        return $this->startRunTime;
    }

    /**
     * @param \Datetime $startRunTime
     * @return CronTaskHistory
     */
    public function setStartRunTime(\Datetime $startRunTime): CronTaskHistory
    {
        $this->startRunTime = $startRunTime;
        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getEndRunTime(): \Datetime
    {
        return $this->endRunTime;
    }

    /**
     * @param \Datetime $endRunTime
     * @return CronTaskHistory
     */
    public function setEndRunTime(\Datetime $endRunTime): CronTaskHistory
    {
        $this->endRunTime = $endRunTime;
        return $this;
    }

    /**
     * @return bool
     */
    public function isResult(): bool
    {
        return $this->result;
    }

    /**
     * @param bool $result
     * @return CronTaskHistory
     */
    public function setResult(bool $result): CronTaskHistory
    {
        $this->result = $result;
        return $this;
    }

    /**
     * @return string
     */
    public function getExceptionMessage(): string
    {
        return $this->exceptionMessage;
    }

    /**
     * @param string $exceptionMessage
     * @return CronTaskHistory
     */
    public function setExceptionMessage(string $exceptionMessage): CronTaskHistory
    {
        $this->exceptionMessage = $exceptionMessage;
        return $this;
    }

    /**
     * @return CronTask
     */
    public function getCronTask(): CronTask
    {
        return $this->cronTask;
    }

    /**
     * @param CronTask $cronTask
     * @return CronTaskHistory
     */
    public function setCronTask(CronTask $cronTask): CronTaskHistory
    {
        $this->cronTask = $cronTask;
        return $this;
    }
}
