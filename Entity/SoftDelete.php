<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\MappedSuperclass;
use JulienCoppin\GlobalBundle\Interfaces\IDeletable;
use Symfony\Component\Security\Core\User\User;

/**
 * Class SoftDelete
 * @package JulienCoppin\GlobalBundle\Entity
 *
 * @MappedSuperclass
 */
abstract class SoftDelete implements IDeletable
{
    /**
     * @var \Datetime
     *
     * @ORM\Column(name="DeletedAt", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="DeletedBy", type="string", nullable=true, length=255)
     */
    private $deletedBy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Active", type="boolean", nullable=false, options={"default" : false})
     */
    private $active;

    /**
     * @return bool
     */
    public abstract function isDeletable() : bool;

    /**
     * @param $user
     */
    public function callbackDelete(?User $user) : void
    {
        $this->delete($user);
    }

    /**
     * SoftDelete constructor.
     */
    public function __construct()
    {
        $this->deletedAt = null;
        $this->deletedBy = null;
        $this->active = true;
    }

    /**
     * @param $user
     */
    public function delete(?User $user) : void
    {
        if ($this->deletedAt === null) {
            $this->deletedAt = new \DateTime();
            if ($user instanceof User) {
                $this->deletedBy = $user->getUsername();
            } else {
                $this->deletedBy = 'localhost';
            }
            $this->active = false;
        }
    }

    public function restore() : void
    {
        if ($this->isDeleted()) {
            $this->deletedAt = null;
            $this->deletedBy = null;
        }
    }

    /**
     * @return bool
     */
    public function isDeleted() : bool
    {
        return !($this->deletedBy === null && $this->deletedAt === null);
    }

    public function toggleActive() : void
    {
        $this->active = !$this->active;
    }

    /**
     * @return \Datetime
     */
    public function getDeletedAt(): \Datetime
    {
        return $this->deletedAt;
    }

    /**
     * @param \Datetime $deletedAt
     * @return SoftDelete
     */
    public function setDeletedAt(\Datetime $deletedAt): SoftDelete
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeletedBy(): string
    {
        return $this->deletedBy;
    }

    /**
     * @param string $deletedBy
     * @return SoftDelete
     */
    public function setDeletedBy(string $deletedBy): SoftDelete
    {
        $this->deletedBy = $deletedBy;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return SoftDelete
     */
    public function setActive(bool $active): SoftDelete
    {
        $this->active = $active;
        return $this;
    }
}
