<?php

declare(strict_types=1);

namespace JulienCoppin\GlobalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class CronTask
 * @package JulienCoppin\GlobalBundle\Entity
 *
 * @ORM\Table(name="CronTasks")
 * @ORM\Entity(repositoryClass="JulienCoppin\GlobalBundle\Repository\CronTaskRepository")
 */
final class CronTask
{
    /**
     * @var integer
     *
     * @ORM\Column(name="CronTaskID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="CronTaskName", type="string", nullable=false, length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="CronTaskServiceName", type="string", nullable=false, length=255)
     */
    private $serviceName;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="CronTaskRunTime", type="time", nullable=false)
     */
    private $runTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="CronTaskRunDayOfWeek", type="integer", nullable=true)
     */
    private $runDayOfWeek;

    /**
     * @var integer
     *
     * @ORM\Column(name="CronTaskRunDayOfMonth", type="integer", nullable=true)
     */
    private $runDayOfMonth;

    /**
     * @var string
     *
     * @ORM\Column(name="CronTaskTargetEntityNamespace", type="string", nullable=true, length=255)
     */
    private $entityNamespace;

    /**
     * @var string
     *
     * @ORM\Column(name="CronTaskTargetEntityIDField", type="string", nullable=true, length=255)
     */
    private $entityIDField;

    /**
     * @var integer
     *
     * @ORM\Column(name="CronTaskTargetEntityIDValue", type="integer", nullable=true)
     */
    private $targetEntityIDValue;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CronTaskActive", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return CronTask
     */
    public function setId(int $id): CronTask
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return CronTask
     */
    public function setName(string $name): CronTask
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getServiceName(): string
    {
        return $this->serviceName;
    }

    /**
     * @param string $serviceName
     * @return CronTask
     */
    public function setServiceName(string $serviceName): CronTask
    {
        $this->serviceName = $serviceName;
        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getRunTime(): \Datetime
    {
        return $this->runTime;
    }

    /**
     * @param \Datetime $runTime
     * @return CronTask
     */
    public function setRunTime(\Datetime $runTime): CronTask
    {
        $this->runTime = $runTime;
        return $this;
    }

    /**
     * @return int
     */
    public function getRunDayOfWeek(): int
    {
        return $this->runDayOfWeek;
    }

    /**
     * @param int $runDayOfWeek
     * @return CronTask
     */
    public function setRunDayOfWeek(int $runDayOfWeek): CronTask
    {
        $this->runDayOfWeek = $runDayOfWeek;
        return $this;
    }

    /**
     * @return int
     */
    public function getRunDayOfMonth(): int
    {
        return $this->runDayOfMonth;
    }

    /**
     * @param int $runDayOfMonth
     * @return CronTask
     */
    public function setRunDayOfMonth(int $runDayOfMonth): CronTask
    {
        $this->runDayOfMonth = $runDayOfMonth;
        return $this;
    }

    /**
     * @return string
     */
    public function getEntityNamespace(): string
    {
        return $this->entityNamespace;
    }

    /**
     * @param string $entityNamespace
     * @return CronTask
     */
    public function setEntityNamespace(string $entityNamespace): CronTask
    {
        $this->entityNamespace = $entityNamespace;
        return $this;
    }

    /**
     * @return string
     */
    public function getEntityIDField(): string
    {
        return $this->entityIDField;
    }

    /**
     * @param string $entityIDField
     * @return CronTask
     */
    public function setEntityIDField(string $entityIDField): CronTask
    {
        $this->entityIDField = $entityIDField;
        return $this;
    }

    /**
     * @return int
     */
    public function getTargetEntityIDValue(): int
    {
        return $this->targetEntityIDValue;
    }

    /**
     * @param int $targetEntityIDValue
     * @return CronTask
     */
    public function setTargetEntityIDValue(int $targetEntityIDValue): CronTask
    {
        $this->targetEntityIDValue = $targetEntityIDValue;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return CronTask
     */
    public function setActive(bool $active): CronTask
    {
        $this->active = $active;
        return $this;
    }
}
